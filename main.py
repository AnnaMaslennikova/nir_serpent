from search import Search

if __name__ == '__main__':
    print("Начинается поиск mat fuel в текстовом файле...")
    list = Search.Searcher.search_in_txt_file("D:\\for_nir\\VVER-Caz.txt")
    if not list:
        print("Список, содержащий строки для поиска Adens пуст")
    else:
        length = len(list)
        print("Длина списка: " + str(length))
        print("Начинаем искать соответствующие ADENS в файле .m и формировать файлики...")
        Search.Searcher.search_in_m_file(list, "D:\\for_nir\\adens_files\\", "D:\\for_nir\\VVER-Caz.txt_dep.m")
        print("Закончили создавать файлы...")
