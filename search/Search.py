
class Searcher():

    @staticmethod
    def search_in_txt_file(filename):
        """метод ищет в строке, если есть burn соответствующий fuel и
        формирует массив для посика соответствующего ADENS"""
        f = open(filename, 'r')
        l_burns = list() #создаем список строк для fuel - формата: MAT_fuel ... _ADENS
        subs = "fuel"
        substr = "burn"
        pattern_part1 = "MAT_"   # was 'MAT_fuel'
        pattern_part2 = "_ADENS = ["
        for line in f:  # для каждой строки из файла ищем
            if substr in line:  # если строка содержит слово burn
                s = line.split()  # разбиваем ее по пробелам
                for ss in s:  #для каждого элемента разбиенной строки ещем
                    if subs in ss:  #содержит ли элемент подстроку fuel
                        hvost = ss[ss.rfind(subs): len(ss)]  #создаем переменную, которая будет содержать
                                                             # соответствующее число, стоящее после fuel
                l_burns.append(pattern_part1 + hvost + pattern_part2)   # формируем строку формата: MAT_fuel+hvost_ADENS

        return l_burns

    @staticmethod
    def search_in_m_file(l_burns, filesPath, m_filename):
        """Метод ищет в .m файле ADENS-ы и извлекает все, что находится между квадратными скобками, для каждого fuel"""
        last_symbol = "]"
        m_file = open(m_filename, 'r') #открываем .m файл для чтения и нахождения соответствующих ADENS
        text = m_file.read()
        for l in l_burns:  #для каждого элемента массива, содержащего паттерны ADENS
            if l in text:
                start = text.find(l)+ len(l)
                end = text.find(last_symbol, start)
                adens = text[start : end]
                filename = str(filesPath + l + ".txt")
                adens_file = open(filename, "w")
                adens_file.write(adens)
                adens_file.close()


             #   for line in m_file:  # для каждой строки в .m файле
              #      if l in line:  # проверяет, сожержится ли в строке фраза с ADENS
              #          adens = line[line.rfind(l):line.find(last_symbol)]
              #          adens_file = open(filesPath + l + ".txt")
              #          adens_file.write(adens)
               #         adens_file.close()